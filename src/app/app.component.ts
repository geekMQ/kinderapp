import { AdminDaoProvider } from './../providers/admin-dao/admin-dao';
import { Component, ViewChild } from '@angular/core';
import { Platform, MenuController, Nav } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AngularFireAuth } from '@angular/fire/auth';

import { FormGrupoPage } from '../pages/form-grupo/form-grupo';
import { FormNinoPage } from '../pages/form-nino/form-nino';
import { LoginPage } from '../pages/login/login';
import { GraficosPage } from '../pages/graficos/graficos';


import { PayPalPage } from '../pages/paypal/paypal.page'; 

   

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  login: any = LoginPage;
  rootPage:any = LoginPage; 
  PayPalPage:any = PayPalPage;
  crearGrupo:any=FormGrupoPage;
  addNino:any=FormNinoPage;
  graficoPage:any=GraficosPage;
  @ViewChild(Nav) nav: Nav; 

  
  constructor(private adminDao:AdminDaoProvider,private aFAuth: AngularFireAuth, platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, private menControl:MenuController) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
    this.enableAuthenticatedMenu();
  }

  enableAuthenticatedMenu() {
    this.menControl.enable(false);
    
  }
  openPage(page:any){
    
    console.log(page);
    this.nav.push(page);
   // this.nav.setRoot(page);
   // this.rootPage=page;
    this.menControl.close();

  }

  LogOut(){
    this.aFAuth.auth.signOut();
    this.nav.setRoot(this.login);
    this.rootPage = LoginPage;
  }
}
