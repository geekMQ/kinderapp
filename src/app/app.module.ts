import { MisHijosPage } from './../pages/mis-hijos/mis-hijos';
import { DashboardPage } from './../pages/dashboard/dashboard';
import { FormPadrePage } from './../pages/form-padre/form-padre';
import { GrupoDaoProvider } from './../providers/grupo-dao/grupo-dao';

import { PadreDaoProvider } from './../providers/padre-dao/padre-dao';
import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { GraficosPage } from '../pages/graficos/graficos';
import { EstiloAprendizajePage } from '../pages/estilo-aprendizaje/estilo-aprendizaje';
import { AddIntegrantesPage } from '../pages/add-integrantes/add-integrantes';
import { CompartirPage } from '../pages/compartir/compartir';
import { AnotarsePage } from '../pages/anotarse/anotarse';
import { Facebook } from '@ionic-native/facebook';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ToatsAlertProvider } from '../providers/toats-alert/toats-alert';
import { GrupNinosPage } from '../pages/grup-ninos/grup-ninos';
import { ActividadPage } from '../pages/actividad/actividad';

import { LoginPage } from '../pages/login/login';
import { RegisterModalPage } from '../pages/register-modal/register-modal';

import { FormActividadPage } from '../pages/form-actividad/form-actividad';
import { ListadoActividadesPage } from '../pages/listado-actividades/listado-actividades';
import { FormGrupoPage } from '../pages/form-grupo/form-grupo'; 
import { FormNinoPage } from '../pages/form-nino/form-nino';
import { ListadoGruposPage } from '../pages/listado-grupos/listado-grupos';
import { AddPadresPage } from '../pages/add-padres/add-padres';
import { AddEstudiantesPage } from '../pages/add-estudiantes/add-estudiantes';
import { AddPage } from '../pages/add/add';

//providers
import { Toast } from '@ionic-native/toast';
import { InterfacesProvider } from '../providers/interfaces/interfaces';
import { ActividadDaoProvider } from '../providers/actividad-dao/actividad-dao';
import { NinoDaoProvider } from '../providers/nino-dao/nino-dao';
import { UtilitiesProvider } from '../providers/utilities/utilities';
import { SubirImagenProvider } from '../providers/subir-imagen/subir-imagen';

//firebase

import { AngularFireModule } from '@angular/fire';
import { AngularFireDatabaseModule, AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { PipesModule } from '../pipes/pipes.module';

//plujin
import { ImagePicker } from '@ionic-native/image-picker';
import { StarRatingModule } from 'ionic3-star-rating';
import { LoadingController } from 'ionic-angular';

import { SocialSharing } from '@ionic-native/social-sharing';
import { AdminDaoProvider } from '../providers/admin-dao/admin-dao';
import { UserDaoProvider } from '../providers/user-dao/user-dao';
import { GrupoPage } from '../pages/grupo/grupo';
import { ShareAndSubscribeProvider } from '../providers/share-and-subscribe/share-and-subscribe';

//graficos 
import { ChartsModule } from 'ng2-charts';
import { GraficoBarraPage } from '../pages/grafico-barra/grafico-barra';
import { GraficoLineaPage } from '../pages/grafico-linea/grafico-linea';
import { GraficoDonaPage } from '../pages/grafico-dona/grafico-dona';
import { GraficoItemSeleccionadoPage } from '../pages/grafico-item-seleccionado/grafico-item-seleccionado';

//Paypal
import { PayPalModule } from '../pages/paypal/paypal.module';

import { PayPal, PayPalPayment, PayPalConfiguration } from '@ionic-native/paypal';
import { ListaProfesPage } from '../pages/lista-profes/lista-profes';
import { GruposCompartidosPage } from '../pages/grupos-compartidos/grupos-compartidos';




export const firebaseConfig = {

};


@NgModule({
  declarations: [
    RegisterModalPage,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    GraficosPage,
    LoginPage,
    FormNinoPage,
    FormPadrePage,
    GrupNinosPage,
    EstiloAprendizajePage,
    ListadoActividadesPage,
    ActividadPage,
    FormActividadPage,
    FormGrupoPage,
    DashboardPage,
    ListadoGruposPage,
    AddIntegrantesPage,
    AddPadresPage,
    AddEstudiantesPage,
    CompartirPage,
    AddPage,
    GrupoPage,
    MisHijosPage,
    AnotarsePage,
    GraficoBarraPage,
    GraficoLineaPage,
    GraficoDonaPage,
    GraficoItemSeleccionadoPage,
    ListaProfesPage,
    GruposCompartidosPage
    //SearchPipe,
    //SortPipe
  ],
  imports: [
    StarRatingModule,
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(firebaseConfig),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    PipesModule,
    BrowserModule,
    ChartsModule,
    PayPalModule

    ],
  bootstrap: [IonicApp],
  entryComponents: [
    RegisterModalPage,
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    GraficosPage,
    LoginPage,
    DashboardPage,
    FormNinoPage,
    FormPadrePage,
    GrupNinosPage,
    EstiloAprendizajePage,
    ListadoActividadesPage,
    ActividadPage, 
    FormActividadPage,
    FormGrupoPage,
    ListadoGruposPage,
    AddIntegrantesPage,
    AddPadresPage,
    AddEstudiantesPage,
    CompartirPage,
    AddPage,
    GrupoPage,
    MisHijosPage,
    AnotarsePage,
    GraficoBarraPage,
    GraficoLineaPage,
    ListaProfesPage,
    GraficoDonaPage,
    GraficoItemSeleccionadoPage,
    GruposCompartidosPage
  ], 
  providers: [
    Facebook,
    StatusBar,
    SplashScreen,
    Toast,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ToatsAlertProvider,
    InterfacesProvider,
    AngularFireDatabase,
    NinoDaoProvider,
    PadreDaoProvider,
    ImagePicker,
    UtilitiesProvider,
    ActividadDaoProvider,
    GrupoDaoProvider,
    SubirImagenProvider,
    LoadingController,
    SocialSharing,
    AdminDaoProvider,
    UserDaoProvider,
    ShareAndSubscribeProvider,
    PayPal  
  ]
})
export class AppModule {}
