import { NgModule } from '@angular/core';
import { TextoVacioPipe } from './texto-vacio/texto-vacio';
import { SearchPipe } from './search/search';
import { SortPipe } from './sort/sort';


@NgModule({
	declarations: [TextoVacioPipe,
    SearchPipe,
    SortPipe],
	imports: [],
	exports: [TextoVacioPipe,
    SearchPipe,
    SortPipe]
})
export class PipesModule {}
