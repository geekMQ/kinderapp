import { Pipe, PipeTransform } from '@angular/core';


@Pipe({
  name: 'textoVacio',
})
export class TextoVacioPipe implements PipeTransform {

  transform(value: string, text:string="sin texto") {
    return (value)?value:text;
  }
}
