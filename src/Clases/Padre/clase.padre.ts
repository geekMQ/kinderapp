export class ClasePadre{
    nombre: string;
    apellido1: string;
    apellido2: string;
    cedula:string;
    celular:string;
    fechaCreacion:string;
    urlImg:string;
    key:string;

   
    constructor(nombre: string, apellido1: string, apellido2: string
        , cedula:string,    celular:string, fechaCreacion:string, urlImg:string) {
        this.nombre = nombre;
        this.apellido1 = apellido1;
        this.apellido2 = apellido2;
        this.cedula=cedula;
        this.celular=celular
        this.fechaCreacion = fechaCreacion;
        this.urlImg = urlImg;
    }

}