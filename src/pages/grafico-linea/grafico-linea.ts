import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActividadDaoProvider } from '../../providers/actividad-dao/actividad-dao';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { ActividadPage } from '../actividad/actividad';
import { GraficoItemSeleccionadoPage } from '../grafico-item-seleccionado/grafico-item-seleccionado';

/**
 * Generated class for the GraficoLineaPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grafico-linea',
  templateUrl: 'grafico-linea.html',
})
export class GraficoLineaPage {

  
  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(84, 142, 49,0.50)',
      borderColor: '#437f13',
      pointBackgroundColor: '#437f13',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,0,0.8)',
      pointRadius: 9,
      pointHitRadius: 10
      //tension:   0.3
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
  
  
  // events
  public chartClicked(event,arr):void {
    console.log(event.active);
    let element= event.active;
    if (element.length > 0) {
        var value = element[0]._index;//index seleccionado
        this.actividad=this.actDao.arrayVisual[value];
        this.navCtrl.push(GraficoItemSeleccionadoPage,{'actividad':this.actividad,'estilo':'visual'});
    }
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  array:number[]=[];
  
  public lineChartData:Array<any> = [
    {data: this.array, label: 'Visual'}
  ];

  actividad:ClaseAvtividad;
  estilo:'visual';
  terminado:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public actDao: ActividadDaoProvider) {
  
    this.actDao.getAllratingVisual().then((result)=>{

      result.forEach((element:ClaseAvtividad) => {
        let rate=element.valoracion;
       this.array.push(rate);
      });
      this.terminado=true;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraficoLineaPage');
  }

}
