import { FormNinoPage } from './../form-nino/form-nino';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { NinoDaoProvider } from '../../providers/nino-dao/nino-dao';
import { Observable } from '../../../node_modules/rxjs';



@IonicPage()
@Component({
  selector: 'page-mis-hijos',
  templateUrl: 'mis-hijos.html',
})
export class MisHijosPage implements OnInit{

  items: Observable<any[]>;
  url1: string;

  constructor(public navCtrl: NavController, public navParams: NavParams
    ,public ninoDao:NinoDaoProvider) {
      this.url1 = "../../assets/imgs/hijos.png";
  }

  ionViewWillEnter() {
   
    this.ninoDao.getAll();
    this.items= this.ninoDao.items;
  }

  ngOnInit() {
   
  }

  registrarHijo(){
    this.navCtrl.push(FormNinoPage);
  }

}
