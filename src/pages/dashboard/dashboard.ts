import { MisHijosPage } from './../mis-hijos/mis-hijos';
import { ListadoGruposPage } from './../listado-grupos/listado-grupos';
import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGrupoPage } from '../form-grupo/form-grupo';



@IonicPage()
@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage implements OnInit{

  public trips: any;
  public url1: string;
  public url2: string;
  public url3: string;
  public url4: string;

  constructor(public nav: NavController) {
    // set sample data
    this.url1 = "../../assets/imgs/group1.jpg";
    this.url2 = "../../assets/imgs/connect.jpg";
    this.url3 = "../../assets/imgs/member.png";
    this.url4 = "../../assets/imgs/hijos.png";
    this.url1 = "../../assets/imgs/group1.jpg";
  }

  ngOnInit() {
   
  }

  // view trip detail
  misGrupos() {
    this.nav.push(ListadoGruposPage);
  }

  misHijos() {
    this.nav.push(MisHijosPage);
  }

  ionViewWillEnter() {
   
  }
  crearGrupo(){
    this.nav.push(FormGrupoPage);
  }
}
