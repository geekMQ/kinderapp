import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficoDonaPage } from './grafico-dona';

@NgModule({
  declarations: [
    GraficoDonaPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficoDonaPage),
  ],
})
export class GraficoDonaPageModule {}
