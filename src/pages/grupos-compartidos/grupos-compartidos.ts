import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { Observable } from 'rxjs';
import { GrupoDaoProvider } from '../../providers/grupo-dao/grupo-dao';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';
import { GrupoPage } from '../grupo/grupo';
import { UserDaoProvider } from '../../providers/user-dao/user-dao';


@Component({
  selector: 'page-grupos-compartidos',
  templateUrl: 'grupos-compartidos.html',
})
export class GruposCompartidosPage {
  items: Observable<any[]>=this.grupoDao.items;
  misgrupos:string='grupos';
  constructor(public navCtrl: NavController, public navParams: NavParams,public grupoDao:GrupoDaoProvider, public usdao:UserDaoProvider) {

    console.log("compartidos");
      this.grupoDao.misGrupos();
      this.items= this.grupoDao.shared; 
    
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GruposCompartidosPage');
  }
  

  irAct(grupo:ClaseGrupo){
    this.navCtrl.push(GrupoPage,{'grupo':grupo});
  }


}
