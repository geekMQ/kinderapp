import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GruposCompartidosPage } from './grupos-compartidos';

@NgModule({
  declarations: [
    GruposCompartidosPage,
  ],
  imports: [
    IonicPageModule.forChild(GruposCompartidosPage),
  ],
})
export class GruposCompartidosPageModule {}
