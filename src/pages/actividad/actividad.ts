import { Component } from '@angular/core';
import { NavController, NavParams, Events } from 'ionic-angular';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { FormActividadPage } from '../form-actividad/form-actividad';
import { ClaseNino } from '../../Clases/Nino/clase.nino';

@Component({
  selector: 'page-actividad',
  templateUrl: 'actividad.html',
})
export class ActividadPage {


  date:Date=null;
  note: string = "";
  actividad:ClaseAvtividad;
  nino:ClaseNino;
  estilo:string;

  constructor(public navCtrl: NavController,public events: Events, public navParams: NavParams) {
    events.subscribe('star-rating:changed', (starRating) => {console.log(starRating)});

    this.actividad=this.navParams.get('actividad');
    this.estilo=this.navParams.get('estilo');
  }

  editarActividad(){
    this.navCtrl.push(FormActividadPage,{'actividadEditar':this.actividad,'estilo':this.estilo});

  }

}
