import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormNinoPage } from './form-nino';

@NgModule({
  declarations: [
    FormNinoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormNinoPage),
  ],
})
export class FormNinoPageModule {}
