import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { PadreDaoProvider } from '../../providers/padre-dao/padre-dao';
import { Observable } from 'rxjs';
import { ClasePadre } from '../../Clases/Padre/clase.padre';

@Component({
  selector: 'page-add-padres',
  templateUrl: 'add-padres.html',
})
export class AddPadresPage {
  items: Observable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public padreDao:PadreDaoProvider) {
     padreDao.getAll();
     this.items = padreDao.items;
    }

}
