import { AdminDaoProvider } from './../../providers/admin-dao/admin-dao';

import { Component } from '@angular/core';
import { NavController, ModalController, AlertController, MenuController } from 'ionic-angular';
import { RegisterModalPage } from '../register-modal/register-modal';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase';
import { TabsPage } from '../tabs/tabs';
import { Facebook } from '@ionic-native/facebook'

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  authFine = true;
  correo = "" as string;
  contrasena = "" as string;
  connected:boolean=false;
  constructor(
    private facebook: Facebook,
    private modalController: ModalController, 
    public navCtrl: NavController, 
    private aFAuth: AngularFireAuth,
    private alertController: AlertController,
    private menControl:MenuController
  ,private adminDao:AdminDaoProvider) {
      //this.aFAuth.auth.signOut();
      // current user  https://firebase.google.com/docs/auth/web/manage-users
      this.menControl.enable(false);
   // private alertController: AlertController,private menControl:MenuController) {}
  }

  ionViewCanEnter(){
    this.menControl.enable(false);
      firebase.auth().onAuthStateChanged((user)=> {  
        if(user){
          this.adminDao.getKeyFromUID(firebase.auth().currentUser.uid);
         this.redireccionar();
         return false;
        }else{
          //console.log("loguearse");
          return true;
        } 
      }); 
  }

  redireccionar(){ 
    this.menControl.enable(true);
    this.navCtrl.setRoot(TabsPage);
  }
  
  async iniciarSesion(){
    try{ 
      this.authFine = true;    
      const result = await this.aFAuth.auth.signInWithEmailAndPassword(this.correo,this.contrasena);      
    }
    catch(e){
      if(e.message == "There is no user record corresponding to this identifier. The user may have been deleted."){
        let alert = this.alertController.create({
          title: 'Usuario no registrado',
          subTitle: 'El usuario no existe, por favor vuelva a intentarlo',
          buttons: ['Ok']
        });
        alert.present();
      }
      else{
        let alert = this.alertController.create({
          title: 'Contraseña incorrecta',
          subTitle: 'La contraseña es incorrecta, ingresela nuevamente',
          buttons: ['Ok']
        });
        alert.present();
      }
      this.authFine = false;
    }

    if(this.authFine){
      this.adminDao.getKeyFromUID(firebase.auth().currentUser.uid);
      this.navCtrl.setRoot(TabsPage);
      this.menControl.enable(true);
      
    }
    
  }

  registrarse(){
    let openRegisterModal = this.modalController.create(RegisterModalPage);
    openRegisterModal.present();
  }

  registerWithFacebook(){
    this.aFAuth.auth
      .signInWithPopup(new firebase.auth.FacebookAuthProvider())
      .then(res => console.log(res));
    // this.facebook.login(["email"]).then((loginResponse) => {
    //   let credential = firebase.auth.FacebookAuthProvider.credential(loginResponse.authResponse.accessToken);

    //   firebase.auth().signInWithCredential(credential).then((info) => {
    //     console.log(info);
    //   })
    // })
  }
}
