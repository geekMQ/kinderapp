import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddIntegrantesPage } from './add-integrantes';

@NgModule({
  declarations: [
    AddIntegrantesPage,
  ],
  imports: [
    IonicPageModule.forChild(AddIntegrantesPage),
  ],
})
export class AddIntegrantesPageModule {}