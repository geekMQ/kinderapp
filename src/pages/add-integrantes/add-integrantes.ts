import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';
import { AddPadresPage } from '../add-padres/add-padres'; 
import { AddEstudiantesPage } from '../add-estudiantes/add-estudiantes'; 

@Component({
  selector: 'page-add-integrantes',
  templateUrl: 'add-integrantes.html',
})
export class AddIntegrantesPage {
  grupo:ClaseGrupo;
  profesor:string="profesor";
  estudiante:string="estudiante";
  padre:string="padre";

  constructor(public navCtrl: NavController, public navParams: NavParams) {
    this.grupo=this.navParams.get('grupo');
    console.log(this.grupo);
  }
  
  addPadres(){
    this.navCtrl.push(AddPadresPage);
  }

  addEstudiantes(){
    this.navCtrl.push(AddEstudiantesPage);
  }
  
}
