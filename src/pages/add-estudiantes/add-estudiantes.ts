import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NinoDaoProvider } from '../../providers/nino-dao/nino-dao';
import { Observable } from 'rxjs';
import { ClaseNino } from '../../Clases/Nino/clase.nino';

@Component({
  selector: 'page-add-estudiantes',
  templateUrl: 'add-estudiantes.html',
})
export class AddEstudiantesPage {
  items: Observable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public ninoDao:NinoDaoProvider) {
     ninoDao.getAll();
     this.items = ninoDao.items;
    }

}
