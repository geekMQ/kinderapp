import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficoItemSeleccionadoPage } from './grafico-item-seleccionado';

@NgModule({
  declarations: [
    GraficoItemSeleccionadoPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficoItemSeleccionadoPage),
  ],
})
export class GraficoItemSeleccionadoPageModule {}
