import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NinoDaoProvider } from '../../providers/nino-dao/nino-dao';
import { Observable } from 'rxjs';
import { ClaseNino } from '../../Clases/Nino/clase.nino';
import { EstiloAprendizajePage } from '../estilo-aprendizaje/estilo-aprendizaje';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';
import { FormNinoPage } from '../form-nino/form-nino';


@Component({
  selector: 'page-grup-ninos',
  templateUrl: 'grup-ninos.html',
})
export class GrupNinosPage {

  sexo:string;
  grupo:ClaseGrupo;
  items:Observable<any[]>;

  bla;
  constructor(public navCtrl: NavController, public navParams: NavParams
    ,public ninoDao:NinoDaoProvider) {
    // ninoDao.getAll();
    this.grupo=this.navParams.get('grupo');
    this.ninoDao.getNinoByGroup(this.grupo.key);
    this.items=this.ninoDao.items;
    }

    navegarEstilosAprendizaje(nino:ClaseNino){
      
      this.navCtrl.push(EstiloAprendizajePage,{'nino':nino});

    }
     
    agregarNino(){
      console.log('llave enviada '+this.grupo.key);
       this.navCtrl.push(FormNinoPage,{'keygrupo':this.grupo.key});
    }
}
