import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { NinoDaoProvider } from '../../providers/nino-dao/nino-dao';

@Component({
  selector: 'page-lista-profes',
  templateUrl: 'lista-profes.html',
})
export class ListaProfesPage {
  itemNino:any[]=[];
  readyNino:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams, public ninoDao:NinoDaoProvider) {
    console.log("respuesta");
    this.ninoDao.getAll().then((array)=>{
      if(array){
        this.itemNino=this.ninoDao.Allitems;
        this.readyNino=true;
      }else{
        //no hay por ahora
      }
    });
    

  }


}
