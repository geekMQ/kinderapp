import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GraficoBarraPage } from './grafico-barra';

@NgModule({
  declarations: [
    GraficoBarraPage,
  ],
  imports: [
    IonicPageModule.forChild(GraficoBarraPage),
  ],
})
export class GraficoBarraPageModule {}
