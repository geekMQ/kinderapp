import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ActividadDaoProvider } from '../../providers/actividad-dao/actividad-dao';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { ActividadPage } from '../actividad/actividad';
import { GraficoItemSeleccionadoPage } from '../grafico-item-seleccionado/grafico-item-seleccionado';

/**
 * Generated class for the GraficoBarraPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-grafico-barra',
  templateUrl: 'grafico-barra.html',
})
export class GraficoBarraPage {

  public lineChartLabels:Array<any> = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions:any = {
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(140, 45, 198,0.50)',
      borderColor: 'rgb(140, 45, 198)',
      pointBackgroundColor: 'rgb(140, 45, 198)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,0,0.8)',
      pointRadius: 9,
      pointHitRadius: 10
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';
  
  
  // events
  public chartClicked(event,arr):void {
    console.log(event.active);
    let element= event.active;
    if (element.length > 0) {
        var value = element[0]._index;//index seleccionado
        this.actividad=this.actDao.arrayAuditivo[value];
        this.navCtrl.push(GraficoItemSeleccionadoPage,{'actividad':this.actividad,'estilo':'auditivo'})
    }
  }
  
  public chartHovered(e:any):void {
    console.log(e);
  }
  array:number[]=[];
  
  public lineChartData:Array<any> = [
    {data: this.array, label: 'Auditivo'}
  ];

  actividad:ClaseAvtividad;
  estilo:'auditivo';
  terminado:boolean=false;
  constructor(public navCtrl: NavController, public navParams: NavParams,public actDao: ActividadDaoProvider) {
  
    this.actDao.getAllratingAuditivo().then((result)=>{

      result.forEach((element:ClaseAvtividad) => {
        let rate=element.valoracion;
       this.array.push(rate);
      });
      this.terminado=true;
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraficoBarraPage');
  }

}
