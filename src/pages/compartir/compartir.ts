import { Component } from '@angular/core';
import { NavController, NavParams  } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';


@Component({
  selector: 'page-compartir',
  templateUrl: 'compartir.html'
})
export class CompartirPage {

    message: string = "Anotate en mi grupo (nombre) en Kinder app, con el siguiente enlace o usa el codigo para inscribirte";
    file: string = null;
    link: string = null;
    subject: string = null;
    grupo:ClaseGrupo;

  constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing) {
    this.grupo=this.navParams.get('grupo');
  }

  share(){
      this.socialSharing.share(this.message, this.subject, this.file, this.link)
      .then(()=>{

      }).catch(()=>{

      });
  }

}