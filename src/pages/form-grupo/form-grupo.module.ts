import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FormGrupoPage } from './form-grupo';

@NgModule({
  declarations: [
    FormGrupoPage,
  ],
  imports: [
    IonicPageModule.forChild(FormGrupoPage),
  ],
})
export class FormGrupoPageModule {}