import { UtilitiesProvider } from './../../providers/utilities/utilities';
import { Component } from '@angular/core';
import {  NavController, NavParams, Events, LoadingController } from 'ionic-angular';
import { InterfacesProvider, grupo } from '../../providers/interfaces/interfaces';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';
import { GrupoDaoProvider } from '../../providers/grupo-dao/grupo-dao';
import { ToatsAlertProvider } from '../../providers/toats-alert/toats-alert';
import { Keyboard } from 'ionic-angular';
import { ImagePickerOptions, ImagePicker } from '@ionic-native/image-picker';
import { SubirImagenProvider,ArchivoSubir } from '../../providers/subir-imagen/subir-imagen';
import { TabsPage } from '../tabs/tabs';
import { UserDaoProvider } from '../../providers/user-dao/user-dao';


@Component({
  selector: 'page-form-grupo',
  templateUrl: 'form-grupo.html',
})
export class FormGrupoPage {

  grupo:ClaseGrupo;
  enProceso:boolean=false;
  loader:any;
  estilo:string;
  imagenPreviw:string="";
  image64:string="";
 
  constructor(public navCtrl: NavController, public events: Events, public navParams: NavParams,
              public grupoDao:GrupoDaoProvider, public toast:ToatsAlertProvider, 
              private utility: UtilitiesProvider,private keyboard: Keyboard, private imagePicker: ImagePicker,
              public imagDao:SubirImagenProvider,
              public loadingCtrl: LoadingController,public usdao:UserDaoProvider) {

    
    this.estilo=this.navParams.get('estilo');
    this.grupo=this.navParams.get('grupo');

    this.grupo=new ClaseGrupo();

  }

  // se valida si trae imagen o no.
  guardarGrupo(){
    this.enProceso=true;
  
    this. loader = this.loadingCtrl.create({
      content:"Procesando.."
    });   
    this.loader.present();

    if(this.image64){
      alert("subiendo imagen");
      this.subirImagen();
     
    }
    else
    this.subirGrupo();
  }



  subirGrupo(){
    this.grupoDao.save(this.grupo,this.estilo).then((result=>{    
     
      let msj;
        !result? msj="Se ha producido un error al guardar":msj= "Guardado correctamente";
        this.toast.toast_message_bottom(msj); 
  
        if(result){
          this.grupo.inicializar();
          this.imagenPreviw="";
          this.image64="";
        }
          this.enProceso=false;
          this.loader.dismiss();
          this.navCtrl.pop();
    }));
  }

  // permite abrir la galeria de imagenes
  openGallery(){
    let options:ImagePickerOptions={
      maximumImagesCount:1,
      outputType:1,
      quality:70,
    };

    this.imagePicker.getPictures(options).then((results) => {
      for (var i = 0; i < results.length; i++) {
        this.imagenPreviw  = 'data:image/jpeg;base64,' + results[i];
        this.image64  = results[i];
      }
    }, (err) => {
        this.toast.toast_message_bottom("No hemos logrado acceder a la galeria de imagenes...",3000);
     });
}

// permite subir una imagen a firebase y luego guarda la actividad con el url de la imagen
subirImagen(){
    let file:ArchivoSubir={
      titulo:"prueba",
      img:this.image64,
    };    
    this.imagDao.cargarImagen_firebase(file,this.estilo).then((url:string)=>{
      if( url!= null){
       this.grupo.urlImg=url;
       this.subirGrupo();   
      }
    });
}

  nombreGrupo(): string{
    return this.grupo.nombre == '' ? 'Ingresa un nombre.' : this.grupo.nombre;
  }

}