import { Component, ViewChild } from '@angular/core';
import {  NavController, NavParams, Nav } from 'ionic-angular';
import { GraficoBarraPage } from '../grafico-barra/grafico-barra';
import { GraficoLineaPage } from '../grafico-linea/grafico-linea';
import { GraficoDonaPage } from '../grafico-dona/grafico-dona';
import { ActividadDaoProvider } from '../../providers/actividad-dao/actividad-dao';


@Component({
  selector: 'page-graficos',
  templateUrl: 'graficos.html',
})
export class GraficosPage {

  graficoBarra:any = GraficoBarraPage;
  graficoLinea:any = GraficoLineaPage;
  graficoDona:any = GraficoDonaPage; 
  @ViewChild(Nav) nav: Nav;

  openPage(page:any){
    
    console.log(page);
    this.navCtrl.push(page);
   // this.nav.setRoot(page);
   // this.rootPage=page;
    //this.menControl.close();

  }

  constructor(public navCtrl: NavController, public navParams: NavParams) {


  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad GraficosPage');
  }

}
