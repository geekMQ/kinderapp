import { NinoDaoProvider } from './../../providers/nino-dao/nino-dao';
import { Component } from '@angular/core';
import {  NavController, NavParams } from 'ionic-angular';
import { AddPage } from '../add/add';
import { GrupNinosPage } from '../grup-ninos/grup-ninos';

import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';

@Component({
  selector: 'page-grupo',
  templateUrl: 'grupo.html',
})
export class GrupoPage {
  switch:string="Ninos";
  grupo:ClaseGrupo;
  constructor(public navCtrl: NavController, public navParams: NavParams, private ninoDao : NinoDaoProvider) {
    this.grupo=this.navParams.get('grupo');
  }

  
compartir(){
  this.navCtrl.push(AddPage);
}
verNinos(){
  this.navCtrl.push(GrupNinosPage,{'grupo':this.grupo});
}


ionViewWillEnter() {

}



}
