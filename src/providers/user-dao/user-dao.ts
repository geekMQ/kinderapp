import { AdminDaoProvider } from './../admin-dao/admin-dao';
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '../../../node_modules/@angular/fire/database';
import { User } from '../../Clases/Usuario/clase.usuario';
import { Observable } from '../../../node_modules/rxjs';
import { AngularFireAuth } from '@angular/fire/auth';
import firebase from 'firebase';

@Injectable()
export class UserDaoProvider {

  items: Observable<any[]>;
  items2:Observable<any[]>;
  array: any[]=[];
  uid:string=null;
  user:User=null;
  keyEscuela:any=null;
  constructor(public afDB: AngularFireDatabase,private aFAuth: AngularFireAuth, public adminDao: AdminDaoProvider) {
  
  }

  save(user:User, uid: string,image64?:string){

    return new Promise<any>((resolve, reject) => {
      //const newPostKey = firebase.database().ref().child('user').push().key;
      user.key = uid;
    this.afDB.object(`/user/${ uid }`).update( user )
     .then(
          (evt) => 
          {
            resolve( uid );
          },
          (err) =>
          {
             reject(false);
          }
        );//fin then
    });

  }// fin save


  saveProfesor(user:User, uid: string,image64?:string){

    return new Promise<any>((resolve, reject) => {
      //const newPostKey = firebase.database().ref().child('user').push().key;
      user.key = uid;

    this.afDB.object(`/user/${ uid }`).update( user )
     .then(
          (evt) => 
          {
            resolve( uid );
          },
          (err) =>
          {
             reject(false);
          }
        );//fin then
    });

  }// fin save



  getAll(){
    this.items = this.afDB.list('user').valueChanges();      
  }

  //este user contiene el uid, no es el user de el JSON*, solo el id
  setUpUser(){
   /* this.aFAuth.authState.subscribe(user=>{
      if(user) this.uid=user.uid;
      alert(this.uid);
    });*/
   return new Promise((resolve,reject)=>{

          firebase.auth().onAuthStateChanged((user)=> {  
            if(user){   
              this.uid=user.uid;     
              this.getCurrUser(user.uid).then((user)=>{
                if(user){
                  this.keyEscuela=this.user.escuelaKey;
                  resolve(true);
                }
                else
                  reject(false);

              });
      
            }else{
               reject(false);
            }
          });
   });

  }


  getCurrUser(uid:string){
    return new Promise<any>((resolve,reject)=>{     
      const requestRef = firebase.database().ref('user/'+uid);
      requestRef
      .once('value')
      .then((result) =>{
              if(result){                      
                this.user=result.val();
                
                resolve(true);
              }
              else
              reject(false);
        });//end then
    });//end return promise
    
  }

  async getProfesoresFromUID(UID: string){
      this.items=this.afDB.list('user', ref=> ref.orderByChild('escuelaKey').equalTo(this.adminDao.keyAdmin)).valueChanges();
 
   
this.items.forEach(e=>{console.log(e)}) 
  }


  getGroupMembers(institución, grupo){
    this.items2=this.afDB.list(`/grupo/${ institución }/`+grupo, ref=> ref.orderByChild('compartido')).valueChanges();
  }
}
