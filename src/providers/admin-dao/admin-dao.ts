
import { Injectable } from '@angular/core';
import { AngularFireDatabase } from '../../../node_modules/@angular/fire/database';
import { Observable } from '../../../node_modules/rxjs';
import * as firebase from 'firebase';
import { Administrador } from '../../Clases/Admin/clase.admin';
import { Institucion } from '../../Clases/institucion/clase.institucion';

@Injectable()
export class AdminDaoProvider {

  items: Observable<any[]>;
  array: any[]=[];
  admin: Administrador;
  escuela:Institucion;
  keyAdmin: string;

  constructor(public afDB: AngularFireDatabase) {
    this.items = afDB.list('admin').valueChanges();
    // this.getKeyFromUID(firebase.auth().currentUser.uid)
    // .then(result => {this.keyAdmin = result});
  }

  save(userKey:string,image64?:string){

    return new Promise<any>((resolve, reject) => {
      const newPostKey = firebase.database().ref().child('institucion').push().key;
      
      //director y llave propia. que no se ocupa
      this.escuela=new Institucion(userKey,newPostKey);

    this.afDB.object(`/institucion/${ newPostKey }`).update( this.escuela )
     .then(
          (evt) => 
          {
            resolve( newPostKey );
          },
          (err) =>
          {
             reject(null);
          }
        );//fin then
    });

  }// fin save


  getAll(){
    this.items = this.afDB.list('admin').valueChanges();      
  }

  getAdminByID(id: string){
    return new Promise<any>((resolve,reject)=>{
      const requestRef = firebase.database().ref('institucion/'+id)
      .once('value')
      .then((result) =>{
              if(result){                      
                  this.array=[];                        
                  result.forEach(element => {
                    console.log(element);
                    this.array.push(element.val());
                  });     
                  resolve(this.array);
              }
              else
              reject(null);
        });//end then
    });//end return promise
  }

  getKeyFromUID(UID: string){
        return new Promise<any>((resolve,reject)=>{
      const requestRef = firebase.database().ref('institucion');
      requestRef.orderByChild('directorKey')
      .equalTo(UID)
      .once('value')
      .then(
        snapshot => snapshot.val()
      )
      .then(
        data => Object.keys(data)
      ) 
      .then(
        value => {this.keyAdmin = value[0]}
      )
      .catch(
        ()=> console.log("No admin")
      )
    });//end return promise
  }

}
