import { Injectable } from '@angular/core';
import { ClaseGrupo } from '../../Clases/Grupo/clase.grupo';
import * as firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';
import { UserDaoProvider } from '../user-dao/user-dao';

@Injectable()
export class GrupoDaoProvider {
  array: any[]=[];
  items: Observable<any[]>;
  shared:Observable<any[]>;

  constructor(public afDB: AngularFireDatabase,public usdao: UserDaoProvider) {
    
  }

  save(grupo:ClaseGrupo,image64?:string){

    return new Promise<any>((resolve, reject) => {
      const newPostKey = firebase.database().ref().child('grupo').push().key;
      grupo.key=newPostKey;
      if(!this.usdao.user.escuelaKey){
        reject(false);
      }else{
        grupo.escuelaKey=this.usdao.user.escuelaKey;

        this.afDB.object(`/grupo/${ grupo.escuelaKey }/`+newPostKey).update( grupo )
       .then(
            (evt) => 
            {
              resolve( true );
            },
            (err) =>
            {
               reject(false);
            }
          );//fin then
      }

    });

  }

//se necesita el key de la escuela
  getAll(escuela:string){ 
    this.items = this.afDB.list('grupo/'+escuela).valueChanges();
  }

  //solo admin o director como sea
  misGrupos(){

    this.shared=this.afDB.list('grupo/'+this.usdao.keyEscuela, ref=> ref.orderByChild('compartido/'+this.usdao.uid).equalTo(true)).valueChanges();

  }


compartirGrupo(institución, grupo, profesor){
    return new Promise<any>((resolve, reject) => {
      let compartido = {};
      compartido[profesor+""] = true;
        //grupo.escuelaKey=this.usdao.user.escuelaKey;

        this.afDB.object(`/grupo/${ institución }/`+grupo+'/compartido').update( compartido)
       .then(
            (evt) => 
            {
              resolve( true );
            },
            (err) =>
            {
               reject(false);
            }
          );//fin then
      

    });
}


}