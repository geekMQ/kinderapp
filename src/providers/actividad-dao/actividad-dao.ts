import { Injectable } from '@angular/core';
import { ClaseAvtividad } from '../../Clases/Actividad/clase.actividad';
import { AngularFireDatabase } from '@angular/fire/database';
import * as firebase from 'firebase';
import { Observable } from 'rxjs';
import { UserDaoProvider } from '../user-dao/user-dao';

@Injectable()
export class ActividadDaoProvider {
  
  arrayVisual=[];
  arrayAuditivo=[];
  arrayMovimiento=[];

  items: Observable<any[]>;
  constructor(public afDB: AngularFireDatabase,public usdao:UserDaoProvider) {
    console.log('Hello ActividadDaoProvider Provider');
  }


  save(actividad:ClaseAvtividad, tipoActividad:string, image64?:string){

    return new Promise<any>((resolve, reject) => {
      const newPostKey = firebase.database().ref().child('estiloAprendizaje').push().key;
      actividad.key=newPostKey; 

    this.afDB.object(`/estiloAprendizaje/${tipoActividad}/${ newPostKey }`).update( actividad)
     .then(
          (correct) => 
          {
            resolve(true);
          },
          (error) =>
          {
             reject(false);
          }
        );
    });
  }
  

// falta generar que se haga un scroll infinito
  getAll(estilo:string, key:string){    
      this.items=this.afDB.list('estiloAprendizaje/'+estilo, ref=> ref.orderByChild("ninoKey").equalTo(key)).valueChanges();

    /* 
    de esta manera se lee solo una vez la lista sin el value changes(), para no depender del internet
      firebase.database().ref('/estiloAprendizaje/visual').orderByChild('ninoKey').equalTo("LTsUPlnOq77smMX4R79").once('value').then(snapshot => { 

        snapshot.forEach(element => {
          console.log("elemento");
          console.log(element.val());
        });
      });
    */

  }


  updateActividad(actividad: ClaseAvtividad, estilo: string): any {
    return new Promise<any>((resolve, reject) => {    
       
    this.afDB.object(`/estiloAprendizaje/${estilo}/${ actividad.key }`).update(actividad)
     .then(
          (correct) => 
          {
            resolve(true);
          },
          (error) =>
          {
             reject(false);
          }
        );
    });
  }

  getAllratingVisual(){


    return new Promise<any>((resolve,reject)=>{
      const requestRef = firebase.database().ref('estiloAprendizaje/visual');
      requestRef.orderByChild('escuela')
      .equalTo(this.usdao.user.escuelaKey)
      .once('value')
      .then((result) =>{
              if(result){                      
                  this.arrayVisual=[];                        
                  result.forEach(element => {
                    this.arrayVisual.push(element.val());
                  });             
                  resolve(this.arrayVisual);
              }
              else 
              reject(null);
        });//end then
    });//end return promise*/

  }

  getAllratingAuditivo(){


    return new Promise<any>((resolve,reject)=>{
      const requestRef = firebase.database().ref('estiloAprendizaje/auditivo');
      requestRef.orderByChild('escuela')
      .equalTo(this.usdao.user.escuelaKey)
      .once('value')
      .then((result) =>{
              if(result){                      
                  this.arrayAuditivo=[];                        
                  result.forEach(element => {
                    this.arrayAuditivo.push(element.val());
                  });             
                  resolve(this.arrayAuditivo);
              }
              else 
              reject(null);
        });//end then
    });//end return promise*/

  }

  getAllratingMovimiento(){


    return new Promise<any>((resolve,reject)=>{
      const requestRef = firebase.database().ref('estiloAprendizaje/kinestesico');
      requestRef.orderByChild('escuela')
      .equalTo(this.usdao.user.escuelaKey)
      .once('value')
      .then((result) =>{
              if(result){                      
                  this.arrayMovimiento=[];                        
                  result.forEach(element => {
                    this.arrayMovimiento.push(element.val());
                  });             
                  resolve(this.arrayMovimiento);
              }
              else 
              reject(null);
        });//end then
    });//end return promise*/

  }

}
