import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { DeprecatedI18NPipesModule } from '../../../node_modules/@angular/common';

@Injectable()
export class UtilitiesProvider {

  public today: string;

  constructor() {

  this.today = new Date().toISOString();
  
}

}
