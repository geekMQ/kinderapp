import { ClasePadre } from './../../Clases/Padre/clase.padre';
import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import * as firebase from 'firebase';
import { AngularFireDatabase } from '@angular/fire/database';
import { Observable } from 'rxjs';


@Injectable()
export class PadreDaoProvider {

  items: Observable<any[]>;
  constructor(public afDB: AngularFireDatabase) {
    //console.log('Hello PadreDaoProvider Provider');
    this.items = afDB.list('padre').valueChanges();
  }

  save(padre:ClasePadre,image64?:string){

    return new Promise<any>((resolve, reject) => {
      const newPostKey = firebase.database().ref().child('padre').push().key;
      padre.key=newPostKey;

    this.afDB.object(`/padre/${ newPostKey }`).update( padre )
     .then(
          (evt) => 
          {
            resolve( true );
          },
          (err) =>
          {
             reject(false);
          }
        );
    });

  }

  getAll(){
    this.items = this.afDB.list('padre').valueChanges();    

  }

}
